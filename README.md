# Hello World API

Take a simple project through some iterative steps to work on the operational side of the house.
 
Tasks
___

* Initial commit
* Add greeting endpoint
* Add continuous integration
* Add GreetingController and getGreeting() with unit test
* Add operational endpoints via /ops
* Add @RequestParam(name = "name") String name
* Add test coverage reporting - JaCoCo
* Integrate test coverage in CI
* Add linting - SonarLint
* Integrate linting in CI - SonarQube
* Add build with commit Id, expose via /ops/info
* Update @RequestParam with defaultValue
* Move defaultValue to configuration
* Use multiple profiles for configuration per environment
* Externalize configuration with Consul
* Add automated testing - Robot Framework
* Containerize
* Add to Artifactory
* Deploy from Artifactory
* Centralize logs to SumoLogic
* Monitor endpoints with DataDawg
* Add RDBMS
* Add RDBMS Migrations - Flyweight
* Add RDBMS Migrations - Liquibase

