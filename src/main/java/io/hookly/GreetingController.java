package io.hookly;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Author mmather
 */
@RestController
public class GreetingController {

    @GetMapping("/greeting")
    public ResponseEntity<GreetingResponse> getGreeting() {
        val response = new GreetingResponse();
        response.setGreeting("Hello, World!");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

@Getter
@Setter
@ToString
class GreetingResponse{
    private String greeting;
}

